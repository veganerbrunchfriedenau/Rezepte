---
title:      "Schoko-Creme"
categories: [desserts]
tags:       [vollwertkost, rohkost, glutenfrei, zuckerfrei, süss]
author:     robinv
date:       22.05.2019
image:
  path:      /assets/images/Carrot-and-Beetroot_cropped_longer.jpg
#  thumbnail: /assets/images/Carrot-and-Beetroot_thumb.jpg
#  path:      /assets/images/homemade-vegan-nutella-2.jpg
  thumbnail: /assets/images/homemade-vegan-nutella-2.jpg
  caption:   "Photo credit [Davina Hearne](http://davinahearne.com/delicious-raw-beetroot-and-carrot-salad/)"
---

* Hat eine aehnliche konsistenz wie Nutella.
* Haellt sich etwa ein bis zwei Wochen im Kuehlschrank.

## Zutaten

Fuer ca. 1'000g Creme:

* 500g Karotten
* 500g Rote-Bete
* 200g gemahlene Haselnuesse
* 20g Kakao-Pulver
* 200g Dattel-Masse
* (optional) 5g Zimmt

## Zubereitung

1. Karotten & Rote-Bete fein reiben
2. Die geriebenen Rueben kneten, winden und auspressen, und den Saft "vernichten"
3. Den Rest der Zutaten beigeben und einkneten.

