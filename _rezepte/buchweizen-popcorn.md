---
title:      "Buchweizen Popcorn"
categories: [snacks]
tags:       [vollwertkost, glutenfrei, zuckerfrei]
author:     robinv
date:       17.07.2019
image:
  path:     http://nextews.com/images/8d/c4/8dc472e47eb60eda.jpg
---

Schmeckt wie Snickers, ist aber knuspriger, und gesünder!

Buchweizen-Popcorn ist eine ziemlich breit einsetzbare Zutat,
hauptsächlich für süsse Speisen,
aber auch herzhaft nicht zu verschmähen.

Unterschiede zum Mais-Popcorn:

* poppt nur auf ca. die doppelte Grösse (relativ zum ursprünglichen Grösse)
* ist dadurch viel härter
* lässt sich leichter in grösseren mengen in kürzerer Zeit essen
* saugt Flüssigkeit viel langsamer auf
* lässt sich deshalb mit viel mehr Zutaten kombinieren
* da die Oberfläche viel geringer, und die Dichte viel grösser ist,
  ist es praktisch auch mit viel mehr Zutaten kombinierbar, weil ähnlichere Eigenschaften
* wir allgemein als viel wertvoller/gesünder angesehen
* trocknet den Körper nicht so sehr aus

> **Beachte:** Es ist nicht ganz einfach Herzustellen, da es sehr schnell anbrennt.
> Deshalb empfiehlt es sich, es erst mal mit ein paar wenigen Körnern auszuprobieren.

## Zutaten

![Geroesteter Buchweizen](https://www.eatmovefeel.de/images/750x322/shutterstock_134184944.jpg)

- 100g (geroesteter) Buchweizen
- (optional) 1 TL natives Raps-Oel

## Zu beachten

* Wenn das Öl anfängt zu rauchen,
  den Topf kurz vom Herd nehmen,
  und die Hitze ein bisschen runter-schrauben.
* Das Ganze lieber zu früh als zu spät beenden,
  weil gerösteter Buchweizen auch schon ohne weitere Behandlung gegessen werden kann.
  Es ist nur sehr viel Arbeit für die Zähne.
* Bitte die Umrühranleitung genau befolgen, da es sonst sehr leicht passiert,
  dass der Buchweizen schwarz wird, anstatt dass er poppt.

## Anleitung

1. Topf oder Pfanne erhitzen
1. Topf oder Pfanne mit Öl leicht erhitzen
1. Buchweizen hineingeben und unentwegt rühren!\
   Das ist sehr wichtig, da es (mit oder ohne Öl) sehr schnell anbrennt.
   Benutzt am besten einen hitzebeständigen Spatel,
   mit dem ihr versucht sicherzustellen,
   dass kein Korn jemals länger als 2s auf dem Topf-Boden liegt.
1. nach spätestens 30s Öl hinzugeben,
1. nach spätestens 2min ist es fertig,
   und muss sofort in ein anderes Behältnis (kleine Schüssel) gegeben werden,
   damit es nicht anbrennt.

Der gepoppte Buchweizen ist noch sehr hart, und nur etwa doppelt so gross wie in ungepopptem Zustand.
Falls nicht alle Samen aufgepoppt sind, macht das nichts, weil sie innen drin trotzdem schon "transformiert" sind.

