---
title:       Kartoffelsalat
categories:  [herzhaft]
tags:        [salzig]
author:      mira
date:       20.08.2019
image:
  path:      https://url.com/big-image.jpg
...

Allgemeines

## Zutaten

* 1 Pfund Berberkartoffeln
* 1 Pfund frische Erbsen
* 1 Zitrone
* 1 mittelgroße Zwiebel
* Salz, Pfeffer, Senf, Traubenkernöl, Essig

## Zubereitung

1. In einer Salatschüssel aus Zitrone,  Essig, Salz, Pfeffer und etwas Senf (gut mit Schneebesen verrühren und tropfenweise das Öl unter Weiterrühren hinzugeben) eine Vinegrette bereiten. 
2. Mit dem Wiegemesser eine halbe Zwiebel zerkleinern und zur Sauce geben.
3. Kartoffeln und Erbsen getrennt garen
4. Kartoffeln schälen und in Scheiben schneiden
5. mit den Erbsen in die Schüssel geben und abmischen. Finito!

