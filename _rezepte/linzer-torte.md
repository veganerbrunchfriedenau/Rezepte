---
title:      "Feigen-Linzer-Torte"
excerpt:    "Alles was das herz begehrt"
categories: [desserts]
tags:       [vollwertkost, rohkost, glutenfrei, zuckerfrei, süss]
author:     robinv
date:       22.06.2019
image:
  path:      /assets/images/recipe_linzer-torte-3_cropped_long.jpg
  thumbnail: /assets/images/recipe_linzer-torte-1.jpg
---

{% include toc %}

{% comment %}
- 2 cups gluten-free flour
- 4.5 ounces ground almonds or blanched almonds
- 4.5 ounces hazelnuts, chopped and toasted
- 4.5 ounces coconut sugar
- 2 cups vegan butter
- 7 ounces jam
- 2 ounces agave nectar
- 2 teaspoons cinnamon or to taste
- 1/4 teaspoon ginger
- 2-4 cloves or ground cloves
- Orange zest from 1 orange
- 1 tablespoon gluten-free flour mixed with 3 tablespoons water mixed and 1 tablespoon ground flax seeds (optional)
- 2 teaspoons baking soda
- A pinch of salt
- Non-dairy milk, for brushing
- Almond slivers for garnish (optional)

or

270 gram white flour
40 gram ground almonds
3 gram ground cinnamon
7 gram cacao powder
3 gram baking powder
a pinch of salt
130 gram refined coconut oil
130 gram sugar
9 (15mL) TBS of Aquafaba (the liquid from a can of chickpeas) (for a total of 135mL) + 1 TBS of cornstarch slightly whisked
400 grams of tangy jam (I used raspberry, cherry, and rhubarb jam)
{% endcomment %}

Knusprig, deftig, roh und lecker

## Zutaten

* 270g (white) flour
* 40g ground almonds
* 3g ground cinnamon
* 7g cacao powder
* 3g baking powder
* salt
* 130g coconut oil ( :/ )
* 130g sugar (replace with dates or figs or even just more of the mermelade itsself)
* 9g/15ml Aquafaba (the liquid of a can of Chickpeas)

