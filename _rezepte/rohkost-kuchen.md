---
title:      "Raw Cake [EN]"
lang:       en
categories: [desserts]
tags:       [vollwertkost, rohkost, zuckerfrei, süss]
author:     robinv
date:       22.06.2019
image:
  path:      /assets/images/recipe_rohkost-kuchen-2_cropped_long.jpg
  thumbnail: /assets/images/recipe_rohkost-kuchen-3.jpg
---

# Crust (Boden)

## Ingredients

* water
* date-paste
* cacao-powder (just a bit to make it dark)
* oats (soaked over night)

## Steps

1. mix
2. spread out
3. put in fridge

# Filling

## Ingredients

- iron & anti-oxidants
	* 1 cup of cashew nuts (soaked)
	* beetroot (Rote Bete)
	* berries or date-paste
	* cinnamon
- fruit-pudding
	* tofu
	* avocado
- sweet vegetables
	* 500g carrots
	* 1 cup of nuts (soaked)
	* date-paste
	* cinnamon
- bananas
	* 3 frozen, very ripe bananas
	* date-paste
- crunchy (NON RAW!)
	* millet or amaranth cooked and cooled out
	* ... something(s) to give it taste


# Topping

fruit of any kind!

