---
title:      "Zucchini-Basilikum-Quiche"
categories: [herzhaft]
tags:       [zuckerfrei, salzig]
author:     mira
date:       11.08.2019
image:
  path:      https://www.madamecuisine.de/wp-content/uploads/2016/07/zucchini-quiche031.jpg
---

## Zutaten 

### Für den Teig

- 300 g Dinkelmehl 1050
- 200 g Alsan Margarine kalt
- Ca. 4-6 Eßl. Wasser
- ½ Teel. Salz


### Füllung

- 3-4 Zucchinis
- 1 kleine Zwiebel
- Etwas Olivenöl
- 400 g Sojajoghurt (Sojade)
- 2 Handvoll Basilikum
- 3  Eßl. Speisestärke
- 2 eßl. Hefeflocken (optional)
- 1 Eßl. Tahin
- 2 Eßl. Zitronensaft
- Salz, Pfeffer
- Pinienkerne

## Zubereitung

### Für den Teig

Alle Zutaten schnell zusammen kneten, im Kühlschrank 1 Std. ruhen lassen
Dann eine Quicheform einfetten und mit dem Teig auskleiden

### Für die Füllung

1. Zucchinis grob raspeln
2. Zwiebel schälen, klein würfeln
3. Zwiebelwürfel in Olivenöl anbraten, mit den Zucchinis vermischen
4. Sojade, Basilikum, Speisestärke, Zitronensaft, Tahin, Salz, Pfeffer mit dem Pürierstab fein pürieren
5. Die Zucchinimasse auf den Boden geben, die Basilikumsoße drübergeben, etwas mit den Zucchinis vermischen.
6. Bei 180 Grad 30 min. backen
7. Pinienkerne auf die Quiche streuen und weitere 10 Min. backen
8. Warm servieren
