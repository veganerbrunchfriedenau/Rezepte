---
title:      "Lebkuchen-Plätzchen"
categories: [desserts]
tags:       [süss]
author:     ulrichj
date:       17.11.2019
image:
  path:      /assets/images/recipe_lebkuchen-plaetzchen-1.jpg
---

## Lebkuchen-Teig

50 g Zitronat/Orangeat oder geriebene Schale und 200-250 g getrocknete Aprikosen
sehr klein schneiden und im Mörser zu einem festen Mus quetschen (geht auch ohne),
200 g Mandeln, 100 g Haselnüsse, 100 g Walnüsse fein mahlen. Mit 100 g Zucker,
2 Päckchen Vanillinzucker, 1 Päckchen Backpulver und I Päckchen Lebkuchen-
gewürz mischen. 2 EL Stärke mit Orangensaft glattrühren, zu der Masse geben und
alles zu einem schwach klebrigen Teig kneten. Eventuell mehr Wasser zugeben.
Mit den Händen oder Anrichteformen 20 große oder 50 kleine, flache Küchlein
formen und auf Backpapier legen.
20' bei 160° im vorgeheizten Ofen backen.
Kleine Stücke mit der Oberseite in geschmolzene Schokolade tauchen und auf dem
Backpapier wieder ablegen. Große Stücke mit Schokolade bepinseln. Nach Belieben
mit Mandelblättchen, -splittern, Kokosraspeln bestreuen
In einem geschlossenen Gefäß aufbewahren. Sollten sie nicht mehr weich sein, kann
man für einige Stunden ein feuchtes Tuch in das verschlossene Gefäß legen.
