---
title:      "Buchweizen Frühstück"
categories: [sweets]
tags:       [vollwertkost, glutenfrei, zuckerfrei]
author:     robinv
date:       15.10.2019
#image:
#  path:      /assets/images/recipe_buchweizen_snickers_1_cropped_long.jpg
#  thumbnail: /assets/images/recipe_buchweizen_snickers_2.jpg
---

## Zutaten

- 100g (gerösteter) Buchweizen
- 100g  Dattel-Masse
- 2 kleine Rote-Bete (roh)
- Bratöl
- Priese Zimt

## Anleitung

- [Buchweizen-Popcorn](2019-07-17-buchweizen-popcorn.md) nach [verlinkter Anleitung](
../../../../snacks/2019/07/17/buchweizen-popcorn.html)
- Dattel-Masse mit einem Löffel einarbeiten, solange der Buchweizen noch warm ist
- Zimt dazu geben
- Rote-Bete mittels einer feinen Reibe drüber reiben, und mit dem Löffel vermischen

