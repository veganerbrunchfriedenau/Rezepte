---
title:      "Energie-Riegel"
categories: [desserts]
tags:       [vollwertkost, zuckerfrei, süss]
author:     robinv
date:       22.06.2019
image:
  path:      /assets/images/recipe_energie-riegel-5_cropped_long.jpg
  thumbnail: /assets/images/recipe_energie-riegel-4.jpg
---

Dr. Berrinos Riegel

## Zutaten

- 150g Vollkorn-Haferflocken
- 150g Vollkornmehl
- 80g Mandeln
- 20g Leinsamen
- ½ Teelöffel Zimt
- 1 Prise Salz

## Anleitung

- Alle Zutaten zusammen mahlen
- mit : 80 gr Rosinen, 75 bio Oel  und 180 ml Wasser mischen.
- Ein Viertel Stunde warten und dann  auf das Blech und gleichmässig verteilen (1 cm). Im Ofen (180 für 30 Minuten, Mitte) backen.
- Schnitte schneiden

Man kann auch Sonnenblumen Kerne, Datteln usw. Verwenden.
Mit Walnüssen aufpassen, sie werden schnell bitter wenn zu viel gebacken.

