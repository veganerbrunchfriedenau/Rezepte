---
layout:         page
---

Wahrscheinlich willst du zu den [Rezepten](rezepte).

Aktuelles findest du unter [News](news).

## FAQ

##### Wo sind die Rezepte?

[Hier clicken um zu den Rezepten zu kommen](rezepte).


#### Wo findet es statt?

Im [Nachbarschaftsheim Friedenau in Berlin Schoeneberg](
https://www.nbhs.de/stadtteilarbeit/orte-fuer-die-nachbarschaft/nachbarschaftshaus-friedenau/).


```
Nachbarschaftshaus Friedenau
Holsteinische Straße 30
12161 Berlin
```

[![detailed map with clarifications](assets/images/location.png)](
https://www.openstreetmap.org/way/32847854)

#### Was soll ich mitbringen?

TODO

#### Wie kann ich Rezepte einstellen?

TODO robin

#### Wie kann ich Rezepte editieren?

TODO robin

